from sqlalchemy import Column, String, Integer, sql, Boolean
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()


class User(Base):
    __tablename__ = "users"
    id = Column(Integer, primary_key=True)
    hashed_password = Column(String(40), nullable=False)
    username = Column(String(25), unique=True, nullable=False)
    email = Column(String(40), unique=True, nullable=False)
    working_gmail = Column(String(40), nullable=False)
    is_active = Column(Boolean(), default=False, nullable=False)
